import React from 'react'
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import styles from './Movie.module.css';


export default function Movie({id, coverImg, title, summary, genres}) {
  return (
    <div>
      
      <div className={styles.movie}>
        <img className={styles.movie__img} src={coverImg} alt={title} />
        <h2 className={styles.movie__title}>
          {/* Link 컴포넌트는 브라우저 새로고침 없이 다른페이지로 이동시켜 전체페이지가 재렌더링 되는 것을 막아줌 */}
          <Link to={`/movie/${id}`}>{title}</Link>
        </h2>
                              {/* slice 함수 사용해서 summary의 노출될 글자 수 결정 */}
        <p>{summary.length>235 ? `${summary.slice(0,235)}...` : summary}</p>
        <ul className={styles.movie__genres}>
          <li>
            {genres.map(g =>
              <li key={g}>{g}</li>
            )}
          </li>
        </ul>
      </div>
          
    </div>
  )
}

Movie.propTypes={
  id: PropTypes.number.isRequired,
  coverImg: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  summary: PropTypes.string.isRequired,
  genres: PropTypes.arrayOf(PropTypes.string).isRequired,
}