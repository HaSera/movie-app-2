// import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./routes/Home";
import Detail from "./routes/Detail";

function App() {

  // return <Router>
  //   <Switch>     {/* Switch 컴포넌트는 한 번에 하나의 Route만 렌더링되도록 함 */}
  //     <Route path="/">
  //       <Home />
  //     </Route>
  //   </Switch>
  // </Router>;

  
  // react-router-dom v5 → v6 변경사항
  // 1. Switch 컴포넌트가 Routes 컴포넌트로 대체됨
  // 2. Route 컴포넌트 사이에 자식 컴포넌트를 넣지 않고, element prop에 자식 컴포넌트를 할당

  return <Router>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/movie/:id" element={<Detail />} />
    </Routes>
  </Router>;
}

export default App;
