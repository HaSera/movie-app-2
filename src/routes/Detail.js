import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import styles from './Detail.module.css';

export default function Detail() {

  const [movie, setMovie]=useState([]);
            // useParams는 현재 URL의 path parameter를 받아오는 함수
  const {id} = useParams();
  const getMovie = async()=>{
    const json = await (
      await fetch(`https://yts.mx/api/v2/movie_details.json?movie_id=${id}`)
    ).json();
    // console.log(json);
    setMovie(json.data.movie);
  }
  useEffect(() => {
    getMovie();
  }, []);

  return (
    <div>
      
      <div className={styles.detail}>

        <img className={styles.detail_img} src={movie.large_cover_image} alt={movie.title} />
        <h1 className={styles.detail_title}>{movie.title}</h1>
        <h2 className={styles.detail_spec}><span className={styles.detail_spec_title}>제작연도</span>{movie.year}</h2>
        <h2 className={styles.detail_spec}><span className={styles.detail_spec_title}>상영 시간</span>{movie.runtime}분</h2>
        <h2 className={styles.detail_spec}><span className={styles.detail_spec_title}>별점</span>{movie.rating}</h2>
        <h2 className={styles.detail_spec}><span className={styles.detail_spec_title}>좋아요</span>{movie.like_count}</h2>
        <h2 className={styles.detail_spec}><span className={styles.detail_spec_title}>장르</span>
          <ul className={styles.genre_ul}>
            <li className={styles.genre_list} key={id}>{movie.genres}</li>
          </ul>
          {/* <ul className={styles.genre_ul}>
            <li className={styles.genre_list}>
              {movie.genres.map(dg=>
                <li className={styles.genre_list} key={dg}>{dg}</li>
              )}
            </li>
          </ul> */}
        </h2>
        <p className={styles.detail_summary}>{movie.description_intro}</p>

      </div>

    </div>
  );
}
