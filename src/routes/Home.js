import React from 'react'
import Movie from "../components/Movie";
import {useEffect, useState} from "react";
import styles from './Home.module.css';

export default function Home() {
  const [loading, setLoading]=useState(true);
  const [movies, setMovies]=useState([]);
  const [search, setSearch]=useState([]);

  const getMovies = async() => {
    // fetch 함수
    const response = await fetch(
      `https://yts.mx/api/v2/list_movies.json?minimum_rating=8.0&sort_by=year&page=1`
    );
    const json = await response.json();
    setMovies(json.data.movies);
    setLoading(false);
  }

  // useEffect는 useEffect(callBackFunc,dependencies)로 두 개의 인자를 넣어 호출 가능
  // 두번째 인자 생략할 경우 : 렌더링 지속적으로 발생 (지양)
  // 두번째 인자에 빈배열 넣을 경우 : 컴포넌트 최초 렌더링 시 한 번 실행
  useEffect(()=>{
    // fetch(`https://yts.mx/api/v2/list_movies.json?minimum_rating=8.8&sort_by=year`
    // ).then((response) => response.json())
    // .then((json) => {
    //   setMovies(json.data.movies);
    //   setLoading(false);
    // });
    getMovies();
  },[]);

  // 전체 영화 정보가 담긴 json 데이터 호출
  const getSearch = async() => {
    const json = await (
      await fetch(`https://yts.mx/api/v2/list_movies.json`
      )).json();
      setSearch(json.data.movies);
  }
  useEffect(()=>{
    getSearch();
  },[]);

  const [keyword, setKeyword] = useState("");

  const onSubmit = (e) => {
    if (keyword===''){
      alert("검색어를 입력해주세요");
      return
    } /* else if {
      setKeyword(e.target.value);
    } */
  }
  

  return (

    // module.css로 작성시 className은 {props.클래스명}으로 적어줌
    <div className={styles.container}>

      {/* json 데이터로부터 movies 목록을 받아와 key값으로 연결해 화면에 movies의 타이틀을 표시함 */}
      {loading ? 
        <h1 className={styles.loader}>Loading...</h1>
         : 
        <div className={styles.movies}>

          <form onSubmit={onSubmit}>
            <input 
              className={styles.searchBox} 
              type="search" 
              placeholder="검색할 내용을 입력하세요" 
              value={keyword} 
              onChange={(e)=>setKeyword(e.target.value)}
            ></input>
            <button 
              className={styles.searchBtn} 
              type="submit"
            >검색</button>
          </form><br />

          {movies.map(movie => 
          <Movie
            // map을 쓸 때마다 key값을 전달해줘야 함 -> map 안에서 컴포넌트들을 렌더링할 때 사용
            key={movie.id}
            id={movie.id}
            coverImg={movie.medium_cover_image}
            title={movie.title}
            summary={movie.summary}
            genres={movie.genres}
            />
          )}
        </div>}
    </div>
  );
}
